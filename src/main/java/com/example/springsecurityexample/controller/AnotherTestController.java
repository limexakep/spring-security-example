package com.example.springsecurityexample.controller;

import com.example.springsecurityexample.annotation.CustomController;
import com.example.springsecurityexample.exception.BusinessException;
import com.example.springsecurityexample.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class AnotherTestController {

    @GetMapping("")
    public ResponseEntity<Object> throwAnotherException(
            @RequestParam(name = "business", defaultValue = "false", required = false) boolean business
    ) {
        if (business) {
            throw new BusinessException("This is our exception");
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
