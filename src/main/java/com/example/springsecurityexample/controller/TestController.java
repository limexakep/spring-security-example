package com.example.springsecurityexample.controller;

import com.example.springsecurityexample.annotation.CustomController;
import com.example.springsecurityexample.exception.BusinessException;
import com.example.springsecurityexample.exception.CustomException;
import com.example.springsecurityexample.exception.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("")
    public String sayHello(){
        return "Hello";
    }

    @GetMapping("/user")
    public String sayUser(){
        return "User";
    }

    @GetMapping("/admin")
    public String sayAdmin(){
        return "Admin";
    }

    @GetMapping("/exception")
    public ResponseEntity<Object> throwException(
            @RequestParam(name = "business", defaultValue = "false", required = false) boolean business
    ) {
        if (business) {
            throw new CustomException("This is our exception");
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
