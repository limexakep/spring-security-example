package com.example.springsecurityexample.exception;

import com.example.springsecurityexample.annotation.CustomController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = CustomController.class)
public class TestAdvice {
    @ExceptionHandler({BusinessException.class, CustomException.class})
    public ResponseEntity<Response> handleBusinessException(Exception ex) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(
                new Response(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY)
        );
    }
}
