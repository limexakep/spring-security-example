package com.example.springsecurityexample.exception;

import com.example.springsecurityexample.controller.TestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = {TestController.class})
public class AnotherTestAdvice {
    @ExceptionHandler({BusinessException.class, CustomException.class})
    public ResponseEntity<Response> handleBusinessException(Exception ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new Response(ex.getMessage(), HttpStatus.BAD_REQUEST)
        );
    }
}
